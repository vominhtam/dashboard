import {
  APP_LOAD, CREATE_PRODUCT_FAIL, CREATE_PRODUCT_REQUEST, CREATE_PRODUCT_SUCCESS, GET_CATEGORY_FAIL,
  GET_CATEGORY_SUCCESS, GET_PRODUCT_DETAIL_REQUEST, GET_PRODUCT_DETAIL_SUCCESS, GET_PRODUCT_FAIL,
  GET_PRODUCT_SUCCESS,
  LOGIN_FAIL, LOGIN_REQUEST,
  LOGIN_SUCCESS,
  APP_LOADED
} from '../constants/actionTypes';

export default (state = {
  isLogged: false,
  isLoggingIn: false,
  errorMessage: '',
  token: '',
  isLoading: true,
  isGettingProduct: true,
  isGettingCategory: true,
  isCreating: false,
  arrayProduct: [],
  arrayCategory: [],
  productDetail: {},
  isGetProductDetailDone: false,
}, action) => {
  switch (action.type) {
    case LOGIN_FAIL:
      return {
        ...state,
        isLogged: false,
        isLoggingIn: false,
        errorMessage: 'Username or password incorrect',
      };
    case APP_LOAD:
      return {
        ...state,
        isLoading: false,
        isLogged: true,
        isLoggingIn: false,
        token: action.token,
      };
    case APP_LOADED:
      return {
        ...state,
        isLoading: false,
      };
    case GET_PRODUCT_SUCCESS:
      return {
        ...state,
        arrayProduct: action.arrayProduct,
        isGettingProduct: false,
      };
    case GET_PRODUCT_DETAIL_SUCCESS:
      return {
        ...state,
        productDetail: action.productDetail,
        isGetProductDetailDone: true,
        arrayCategory: action.arrayCategory,
        isGettingCategory: false,
      };
    case GET_PRODUCT_DETAIL_REQUEST:
      return {
        ...state,
        isGetProductDetailDone: false,
        isGettingCategory: true,
      };
    case GET_PRODUCT_FAIL:
      return {
        ...state,
        arrayProduct: [],
        isGettingProduct: false,
      };
    case GET_CATEGORY_SUCCESS:
      return {
        ...state,
        arrayCategory: action.arrayCategory,
        isGettingCategory: false,
      };
    case GET_CATEGORY_FAIL:
      return {
        ...state,
        arrayCategory: [],
        isGettingCategory: false,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLogged: true,
        isLoggingIn: false,
        errorMessage: '',
        token: action.token,
      };
    case CREATE_PRODUCT_REQUEST:
      return {
        ...state,
        isCreating: true,
        isCreateDone: false,
        errorMessageCreate: '',
      };
    case CREATE_PRODUCT_SUCCESS:
      return {
        ...state,
        isCreating: false,
        isCreateDone: true,
      };
    case CREATE_PRODUCT_FAIL:
      return {
        ...state,
        isCreating: false,
        isCreateDone: false,
        errorMessageCreate: 'Cannot create',
      };
    case LOGIN_REQUEST:
      return {
        ...state,
        isLogged: false,
        isLoggingIn: true,
        errorMessage: '',
      };
    default:
      return state;
  }
};
