import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { routerMiddleware } from 'react-router-redux';
import { createBrowserHistory as createHistory } from 'history'
import createSagaMiddleware from 'redux-saga';

import reducer from '../reducers';
import IndexSagas from '../sagas';
import { localStorageMiddleware } from './middleware';

export const history = createHistory();

const myRouterMiddleware = routerMiddleware(history);

const sagaMiddleware = createSagaMiddleware();

const getMiddleware = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(myRouterMiddleware, sagaMiddleware, localStorageMiddleware);
  } else {
    // Enable additional logging in non-production environments.
    return applyMiddleware(myRouterMiddleware, sagaMiddleware, localStorageMiddleware, createLogger());
  }
};


export const store = createStore(
  reducer, composeWithDevTools(getMiddleware()));

sagaMiddleware.run(IndexSagas);
