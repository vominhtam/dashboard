import { APP_LOAD, LOGIN_SUCCESS } from '../constants/actionTypes';
import API from './request';

const localStorageMiddleware = store => next => action => {
  if (action.type === LOGIN_SUCCESS) {
    localStorage.setItem('token', action.token);
    localStorage.setItem('refresh', action.refresh);
    API.setToken(action.token);
  }
  if (action.type === APP_LOAD) {
    API.setToken(action.token);
  }
  next(action);
};


export { localStorageMiddleware }
