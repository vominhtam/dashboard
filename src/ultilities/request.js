import axios from 'axios';
import { API } from '../constants/serverConfig';
import handleError from './errorHandle';

const checkDataResponse = data => {
  if (data instanceof Error) {
    return handleError({
      code: 1,
    });
  }
  if ((data instanceof Error || data.code !== 0) && !data.token && !data.data && data.code) {
    return handleError(data);
  }
  return data;
};

class RequestAPI {
  constructor() {
    this.headers = {};
    this.token = null;
  }

  abort = () => {
    if (this._requestController) {
      this._requestController.abort();
      this._requestController = null;
    }
  };

  setToken = token => {
    this.token = token;
  };

  getToken = () => this.token;

  request = (url, init) => {
    let _init = init || { method: 'GET' };
    _init.headers = Object.assign({}, this.headers, _init.headers || {});
    if (this.token) {
      _init.headers.Authorization = `Bearer ${this.token}`;
    }

    return Promise.race([fetch(url, _init)])
      .then(res => {
        if (res.status === 401) {
          return {
            code: 1,
            status: 401,
          };
        }
        res = res ? res.json() : null;
        return checkDataResponse(res);
      })
      .catch(error => checkDataResponse(error));
  };

  login = body => {
    const init = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    return this.request(`${API}/auth/jwt/create`, init);
  };

  createProduct = body => {
    const init = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'App-Id': 1,
        'Content-Type': 'application/json',
      },
    };
    return this.request(`${API}/admin/products`, init);
  };

  updateProduct = body => {
    const init = {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: {
        'App-Id': 1,
        'Content-Type': 'application/json',
      },
    };
    return this.request(`${API}/admin/products/${body.id}`, init);
  };

  getProductRequest = body => {
    const init = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'App-Id': 1,
      },
    };

    return this.request(`${API}/admin/products?is_all=true`, init);
  };

  getProductDetailRequest = id => {
    const init = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'App-Id': 1,
      },
    };

    return this.request(`${API}/admin/products/${id}`, init);
  };

  getCategoryRequest = body => {
    const init = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'App-Id': 1,
      },
    };

    return this.request(`${API}/admin/categories?is_all=true`, init);
  }
}

export default new RequestAPI();
