import { put, call } from 'redux-saga/effects';
import {
  CREATE_PRODUCT_FAIL,
  CREATE_PRODUCT_SUCCESS,
  GET_CATEGORY_FAIL,
  GET_CATEGORY_SUCCESS, GET_PRODUCT_DETAIL_REQUEST,
  GET_PRODUCT_FAIL, GET_PRODUCT_SUCCESS, LOGIN_FAIL, LOGIN_SUCCESS,
  GET_PRODUCT_DETAIL_SUCCESS,
  GET_PRODUCT_DETAIL_FAIL,
} from '../constants/actionTypes';

const loginRequest = function* (api, params) {
  let response = yield call(api.login, params.objectData);
  console.log(response);
  if (response.status && response.code !== 1) {
    yield put({
      type: LOGIN_SUCCESS,
      token: response.data.access,
      refresh: response.data.refresh,
    });
  } else {
    yield put({
      type: LOGIN_FAIL,
    });
  }
};


const createProductRequest = function* (api, params) {
  let response;
  if (params.data.id) {
    response = yield call(api.updateProduct, params.data);
  } else {
    response = yield call(api.createProduct, params.data);
  }
  if (response.status && response.code !== 1) {
    yield put({
      type: CREATE_PRODUCT_SUCCESS,
    });
  } else {
    yield put({
      type: CREATE_PRODUCT_FAIL,
    });
  }
};

const getProductRequest = function* (api, params) {
  let response = yield call(api.getProductRequest);
  if (response.status && response.code !== 1) {
    yield put({
      type: GET_PRODUCT_SUCCESS,
      arrayProduct: response.data,
    });
  } else {
    yield put({
      type: GET_PRODUCT_FAIL,
    });
  }
};


const getProductDetailRequest = function* (api, params) {
  let response = yield call(api.getProductDetailRequest, params.id);
  let responseCategory = yield call(api.getCategoryRequest);
  let arrayCategory = [];
  if (responseCategory.status && responseCategory.code !== 1) {
    arrayCategory = responseCategory.data;
  }
  if (response.status && response.code !== 1) {
    yield put({
      type: GET_PRODUCT_DETAIL_SUCCESS,
      productDetail: response.data,
      arrayCategory: arrayCategory,
    });
  } else {
    yield put({
      type: GET_PRODUCT_DETAIL_FAIL,
    });
  }
};

const getCategoryRequest = function* (api, params) {
  let response = yield call(api.getCategoryRequest);
  if (response.status && response.code !== 1) {
    yield put({
      type: GET_CATEGORY_SUCCESS,
      arrayCategory: response.data,
    });
  } else {
    yield put({
      type: GET_CATEGORY_FAIL,
    });
  }
};

export { loginRequest, getProductRequest, getCategoryRequest, createProductRequest, getProductDetailRequest };
