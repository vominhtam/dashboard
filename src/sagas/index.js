import { takeLatest, all, call, put } from 'redux-saga/effects';
import Api from '../ultilities/request';
import {
  LOGIN_REQUEST, GET_PRODUCT_REQUEST, GET_CATEGORY_REQUEST, CREATE_PRODUCT_REQUEST, GET_PRODUCT_DETAIL_REQUEST
} from '../constants/actionTypes';
import {
  loginRequest,
  getProductRequest,
  getCategoryRequest,
  createProductRequest,
  getProductDetailRequest,
} from './login';

const sagas = [];

const callSagas = sagas.map(saga => call(saga));

const root = function* root() {
  yield all([
    takeLatest(LOGIN_REQUEST, loginRequest, Api),
    takeLatest(GET_PRODUCT_REQUEST, getProductRequest, Api),
    takeLatest(GET_CATEGORY_REQUEST, getCategoryRequest, Api),
    takeLatest(CREATE_PRODUCT_REQUEST, createProductRequest, Api),
    takeLatest(GET_PRODUCT_DETAIL_REQUEST, getProductDetailRequest, Api),
    ...callSagas,
  ]);
};

export default root
