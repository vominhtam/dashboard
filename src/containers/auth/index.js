import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { actionLogin } from '../../actions/login';

import './style.scss';

const mapStateToProps = state => ({
  isLogged: state.Auth.isLogged,
  isLoading: state.Auth.isLoggingIn,
  errorMessage: state.Auth.errorMessage,
});

const mapDispatchToProps = dispatch => ({
  onLogin: objectData => dispatch(actionLogin(objectData)),
});

class AuthComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isDisabled: true,
      isError: false,
    };

  }

  _checkIslLogged = () => {
    let { isLogged } = this.props;

    if (isLogged) {
      return <Redirect to="/" />;
    } else {
      return this._renderContent();
    }
  };

  _handleLogin = () => {
    let { username, password } = this.state;
    let { onLogin } = this.props;
    onLogin({ username, password });
  };

  _handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    }, () => {
      this._checkEnableButton();
    });
  };

  _checkEnableButton = () => {
    let { username, password } = this.state;
    if (username && password) {
      this.setState({
        isDisabled: false,
      });
    } else {
      this.setState({
        isDisabled: true,
      });
    }
  };

  _renderContent = () => {
    let { location, isLoading, errorMessage } = this.props;
    let { username, password, isDisabled } = this.state;
    let pathName = location.pathname;
    let page = pathName.split('/')[1];
    let layout = null;
    let isError = false;
    if (errorMessage) {
      isError = true;
    }

    /*if (page === 'auth') {
      layout = <Auth />;
    } else if (page === 'register') {
      layout = <Register />
    } else {
      layout = <Login />
    }*/
    return (
      <Grid
        container
        alignItems="center"
        direction="row"
        justify="center"
        className="auth-container">
        <div className="paper">
          <Avatar className="avatar">
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className="form" noValidate>
            {errorMessage ? (
              <Typography component="span" className="error-message">
                {errorMessage}
              </Typography>
            ) : null}
            <TextField
              error={isError}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Username"
              name="username"
              autoComplete="Username"
              autoFocus
              value={username}
              onChange={e => this._handleChange(e)}
            />
            <TextField
              error={isError}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={password}
              onChange={e => this._handleChange(e)}
            />
            {
              isLoading ? (
                <div className="loading-container">
                  <CircularProgress />
                </div>
              ) : (
                <Button
                  onClick={() => this._handleLogin()}
                  fullWidth
                  variant="contained"
                  color="primary"
                  className="submit"
                  disabled={isDisabled}
                >
                  Sign In
                </Button>
              )
            }

          </form>
        </div>
      </Grid>
    );
  };


  render() {
    let layout = this._checkIslLogged();
    return layout;

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthComponent);
