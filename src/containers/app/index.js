import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';

import { Spinner } from '../../components/ui';

import Main from '../main';
import Auth from '../auth';
import { actionLoadApp, actionLoadedApp } from '../../actions/login';

import './style.scss';


const mapStateToProps = state => ({
  isLoading: state.Auth.isLoading,
});


const mapDispatchToProps = dispatch => ({
  onLoadApp: (token) => dispatch(actionLoadApp(token)),
  onLoadedApp: () => dispatch(actionLoadedApp()),
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    let { onLoadApp, onLoadedApp } = this.props;
    let token = localStorage.getItem('token');
    if (token) {
      onLoadApp(token);

    } else {
      onLoadedApp();
    }
  }

  render() {
    let layout = <Spinner size={100} />;
    let { isLoading } = this.props;
    let { openToast, messageToast, typeToast, bookingId } = this.state;
    if (!isLoading) {
      layout = (
        <div className="app-container">
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/product" component={Main} />
            <Route exact path="/product-detail/:id" component={Main} />
            <Route exact path="/product/add" component={Main} />
            <Route exact path="/auth" component={Auth} />
          </Switch>
        </div>

      );
    }
    return layout;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
