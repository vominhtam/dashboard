import React, { PureComponent } from 'react';

// Styles
import './style.scss';

export default class Image extends PureComponent {
  static defaultProps = {
    src: null,
    path: null,
    extension: 'svg',
    className: '',
    alt: '',
    style: {},
    mode: 'internal',
  };

  _renderImage = () => {
    let { src, path, extension, className, alt, style, mode, onClick, id } = this.props;
    const DEFAULT_IMG = '/media/images/default/not-found.svg';

    if (!src && !path && !style) {
      return null;
    }

    if (mode === 'internal') {
      src = `/media/images/${path}.${extension}`;
    }

    return (
      <img
        id={id}
        className={`image-container ${className ? '' + className : ''}`}
        src={src}
        onError={e => {
          e.target.src = '';
        }}
        style={style}
        alt={alt || 'media-object'}
      />
    );
  };

  render() {
    return this._renderImage();
  }
}
