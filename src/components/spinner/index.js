import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

import './style.scss';

export default class Spinner extends PureComponent {
  render() {
    let props = this.props;
    return (
      <CircularProgress
        className={`spinner-container ${props.className ? props.className : ''}`}
        size={props.size ? props.size : 10}
        color={props.color ? props.color : 'primary'}
        style={props.style ? props.style : {}}
      />
    )
  }
}