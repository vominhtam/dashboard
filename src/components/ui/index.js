import Spinner from '../spinner';
import Image from '../image';
import Button from '../button';
import ImageSwipe from '../ImageSwiper';

export {
  Spinner,
  Image,
  Button,
  ImageSwipe,
};
