import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Image from '../image';

import './style.scss';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
    paddingTop: '10px',
    minHeight: '80px',
  },
  grayBackground: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: '#f6f6f6',
    paddingTop: '10px',
    minHeight: '80px',
  },
  gridList: {
    paddingLeft: '15px',
    paddingRight: '15px',
    width: '100%',
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
    // overflow: 'hidden',
    position: 'relative',
    textAlign: 'center',
  },
  gridItem: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '100% !important',
    height: '100% !important',
    textAlign: 'center',
    backgroundColor: '#f6f6f8',
    padding: '5px',
  },
  gridItemAsset: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '100% !important',
    height: '100% !important',
    textAlign: 'center',
	    backgroundColor: '#EEEEEE',
    padding: '10px',
  },
  gridItemButton: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '100% !important',
    height: '100% !important',
    textAlign: 'center',
    paddingTop: '13px !important',
  },
  gridItemButtonAsset: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '100% !important',
    height: '100% !important',
    textAlign: 'center',
    paddingTop: '8px !important',
    backgroundColor: '#EEEEEE',
  },
  button: {
    width: '34px',
    height: '34px',
  },
  input: {
    display: 'none',
  },
  icon: {
    width: '34px',
    height: '34px',
  },
  iconAction: {
    width: '9px',
    height: '15px',
  },
  btnRight: {
    position: 'absolute',
    top: '45%',
    right: '5px',
    width: '9px !important',
    height: '15px !important',
    padding: '0 !important',
    cursor: 'pointer',
  },
  btnLeft: {
    position: 'absolute',
    top: '45%',
    left: '5px',
    width: '9px !important',
    height: '15px !important',
    padding: '0 !important',
    cursor: 'pointer',
  },
  itemContainer: {
    position: 'relative',
    height: '70px !important',
    width: '80px !important',
    padding: '10px !important',
  },
  itemButtonContainer: {
    position: 'relative',
    height: '70px !important',
    width: '80px !important',
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: '10px !important',
  },
  iconContainer: {
    position: 'absolute',
    top: '0',
    right: '0',
    backgroundColor: 'rgba(0, 0, 0, 0.08)',
    padding: '3px',
  },
  iconClose: {
    width: '15px',
    height: '15px',
    color: 'white',
  },
  image: {
    width: '100%',
    height: '100%',
    objectFit: 'contain',
  },

});

const tileData = [
  {
    img: './media/images/ic_launcher.png',
    title: 'Image',
    author: 'author',
  },
];


class ImageSwipe extends Component {
  constructor(props) {
    super(props);
    this.swiper = null;
    this.state = {
      arrayImage: [],
      indexActive: 0,
    };
  }

  componentDidMount() {
  }

  render() {
    let { page, arrayImage, addImage, classes, disabled, isHideButton, removeImage, onClick, onNext, onPrevious, isGrayBackground } = this.props;
    let { indexActive } = this.state;
    return (
      <div className={!isGrayBackground ? classes.root : classes.grayBackground}>
        <GridList className={classes.gridList}>
          {
                        arrayImage.map((image, index) => (
                          <div
                            className={classes.itemContainer}
                            key={index}
                            onClick={() => {
                              if (onClick && typeof onClick === 'function') {
                                onClick(image, index);
                                this.setState({ indexActive: index });
                              }
                            }}
                          >
                            <GridListTile className={page === 'asset' ? classes.gridItemAsset : classes.gridItem}>
                              <img className={classes.image} src={image} alt="" />
                            </GridListTile>
                            {
                                    !isHideButton ? (
                                      <IconButton
                                        onClick={(e) => {
                                          e.stopPropagation();
                                          removeImage(index);
                                        }}
                                        variant="outlined"
                                        className={classes.iconContainer}
                                      >
                                        <CloseIcon className={classes.iconClose} />
                                      </IconButton>
                                    ) : null
                                }

                          </div>
                        ))}
          {
                        arrayImage.length < 4 && !isHideButton ? (
                          <div
                            className={arrayImage.length === 0 ? classes.itemButtonContainer : classes.itemContainer}
                          >
                            <GridListTile className={page === 'asset' ? classes.gridItemButtonAsset : classes.gridItemButton}>
                              <label htmlFor="outlined-button-file">
                                <input
                                  accept="image/*"
                                  className={classes.input}
                                  id="outlined-button-file"
                                  multiple
                                  type="file"
                                  disabled={disabled}
                                  onChange={(e) => {
                                    if (arrayImage.length < 4) {
                                      e.preventDefault();

                                      let reader = new FileReader();
                                      let file = e.target.files[0];

                                      reader.onloadend = () => {
                                        let arrayImageClone = arrayImage;
                                        arrayImageClone.push(reader.result);

                                        addImage(arrayImageClone);
                                      };

                                      reader.readAsDataURL(file);
                                      e.target.value = null;
                                    } else {
                                      alert('Add maximum 4 image');
                                    }
                                  }}
                                />
                                <IconButton
                                  variant="outlined"
                                  component="span"
                                  className={classes.button}
                                >
                                  <Image
                                    className={classes.icon}
                                    path="booking/button@3x"
                                    extension="png"
                                  />
                                </IconButton>
                              </label>
                            </GridListTile>
                          </div>
                        ) : null
                    }
          <div
            className={classes.btnLeft}
            onClick={() => {
              if (onPrevious && typeof onPrevious === 'function') {
                let indexPrevious = indexActive - 1;
                if (indexPrevious < 0) {
                  indexPrevious = arrayImage.length - 1;
                }
                onPrevious(arrayImage[indexPrevious]);
                this.setState({
                  indexActive: indexPrevious,
                });
              }
              // let element = document.getElementById('image-container');
              // let left = element.scrollLeft;
              // element.scrollLeft = left + 50
            }}
          >
            <Image
              className={classes.iconAction}
              path="booking/icon-right@3x"
              extension="png"
            />
          </div>
          <div
            className={classes.btnRight}
            onClick={() => {
              if (onNext && typeof onNext === 'function') {
                let indexNext = indexActive + 1;
                if (indexNext >= arrayImage.length) {
                  indexNext = 0;
                }
                onNext(arrayImage[indexNext]);
                this.setState({
                  indexActive: indexNext,
                });
              }
              // let element = document.getElementById('image-container');
              // let left = element.scrollLeft;
              // element.scrollLeft = left + 50
            }}
          >
            <Image
              className={classes.iconAction}
              path="booking/icon-left@3x"
              extension="png"
            />
          </div>
        </GridList>
      </div>

    );
  }
}

export default withStyles(styles)(ImageSwipe);
