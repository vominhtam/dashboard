import React, { PureComponent } from 'react';
import Button from '@material-ui/core/Button';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import KeyboardVoiceICon from '@material-ui/icons/KeyboardVoice';
import Icon from '@material-ui/core/Icon';
import SaveIcon from '@material-ui/icons/NotificationImportant';
import SvgIcon from '@material-ui/core/SvgIcon';

import './style.scss';

export default class ButtonComponent extends PureComponent {
  renderIcon = () => {
    let { iconPath, iconClassName } = this.props;

    if (iconPath) {
      switch (iconPath) {
        case 'facebook':
          return (
            <SvgIcon className={iconClassName}>
              <path d="M17,2V2H17V6H15C14.31,6 14,6.81 14,7.5V10H14L17,10V14H14V22H10V14H7V10H10V6A4,4 0 0,1 14,2H17Z" />
            </SvgIcon>
          );
        case 'google':
          return (
            <SvgIcon className={iconClassName}>
              <path d="M21.35,11.1H12.18V13.83H18.69C18.36,17.64 15.19,19.27 12.19,19.27C8.36,19.27 5,16.25 5,12C5,7.9 8.2,4.73 12.2,4.73C15.29,4.73 17.1,6.7 17.1,6.7L19,4.72C19,4.72 16.56,2 12.1,2C6.42,2 2.03,6.8 2.03,12C2.03,17.05 6.16,22 12.25,22C17.6,22 21.5,18.33 21.5,12.91C21.5,11.76 21.35,11.1 21.35,11.1V11.1Z" />
            </SvgIcon>
          );
        default:
          return null;
      }
    } else {
      return null;
    }
  };

  render() {
    let props = this.props;
    let disabled;
    if (props.disabled) {
      disabled = true;
    } else {
      disabled = false;
    }
    return (
      <Button
        onKeyPress={props.onKeyPress}
        onClick={props.onClick}
        style={props.style}
        disabled={disabled}
        fullWidth={props.fullWidth}
        className={props.className ? props.className : ''}
        color={props.color ? props.color : 'default'}
        variant={props.variant ? props.variant : 'contained'}
        size={props.size ? props.size : 'medium'}
      >
        {this.renderIcon()}
        <span className={props.textClass ? props.textClass : ''}>{props.text ? props.text : ''}</span>
      </Button>
    );
  }
}
