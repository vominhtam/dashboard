import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import moment from 'moment/moment';
import { history } from '../../ultilities/store';

import TablePaginationActionsWrapped from '../../components/table';

import { Image, Button } from '../../components/ui';

import './style.scss';
import { actionGetProductList } from '../../actions/login';
import Spinner from '../../components/spinner';

const mapStateToProps = state => ({
  arrayProduct: state.Auth.arrayProduct,
  isLoading: state.Auth.isGettingProduct,
});

const mapDispatchToProps = dispatch => ({
  onGetProduct: () => dispatch(actionGetProductList()),
});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
  id += 1;
  return { id, name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      rowsPerPage: 10,
    };
  }

  componentDidMount() {
    let { onGetProduct } = this.props;
    onGetProduct();
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  render() {
    const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    let { arrayProduct, isLoading, i18Next } = this.props;
    const { rowsPerPage, page, type, filterDate, isChange, arrayHistoryFilter } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, arrayProduct.length - page * rowsPerPage);
    let fullWidth = true;
    let status = true;
    return (
      <div>
        {
          isLoading ? (
            <div className="loading-container">
              <CircularProgress />
            </div>
          ) : (
            <Grid
              container
              // direction="row"
              className="history-page"
            >
              <Fab
                onClick={() => {
                  history.push('/product/add');
                }}
                color="primary"
                aria-label="add"
                className="btn-add">
                <AddIcon />
              </Fab>
              <Paper className="history-page">
                <div className="table-wrapper">
                  <Table className="table">
                    <TableHead>
                      <TableRow>
                        <TableCell>{"id"}</TableCell>
                        <TableCell>{"name"}</TableCell>
                        <TableCell>{"manufacturer"}</TableCell>
                        <TableCell>{"sku"}</TableCell>
                        <TableCell>{"price"}</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {arrayProduct.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {

                        return (

                          <TableRow
                            key={row.id}
                            onClick={() => {
                              history.push('/product-detail/' + row.id)
                            }}
                          >
                            <TableCell component="th" scope="row">
                              {row.id}
                            </TableCell>
                            <TableCell>{row.name}</TableCell>
                            <TableCell>{row.manufacturer}</TableCell>
                            <TableCell>{row.sku}</TableCell>
                            <TableCell>{row.price}</TableCell>
                          </TableRow>
                        );
                      })}
                      {emptyRows > 0 && (
                        <TableRow style={{ height: 48 * emptyRows }}>
                          <TableCell colSpan={6} />
                        </TableRow>
                      )}
                    </TableBody>
                    <TableFooter>
                      <TableRow>
                        <TablePagination
                          colSpan={5}
                          count={arrayProduct.length}
                          rowsPerPage={rowsPerPage}
                          page={page}
                          onChangePage={this.handleChangePage}
                          onChangeRowsPerPage={this.handleChangeRowsPerPage}
                          ActionsComponent={TablePaginationActionsWrapped}
                        />
                      </TableRow>
                    </TableFooter>
                  </Table>
                </div>
              </Paper>
            </Grid>
          )
        }
      </div>

    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Product);
