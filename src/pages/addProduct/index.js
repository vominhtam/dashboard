import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import moment from 'moment/moment';
import ButtonComponent from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Card from '@material-ui/core/Card';
import CloseIcon from '@material-ui/icons/Close';
import Input from '@material-ui/core/Input';
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';
import { Image } from '../../components/ui';


import { history } from '../../ultilities/store';
import './style.scss';
import { actionGetCategory, actionCreateProduct, actionGetProductDetail } from '../../actions/login';
import Spinner from '../../components/spinner';

const mapStateToProps = state => ({
  isLoading: state.Auth.isGettingCategory,
  isCreating: state.Auth.isCreating,
  arrayCategory: state.Auth.arrayCategory,
  isCreateDone: state.Auth.isCreateDone,
  isGetProductDetailDone: state.Auth.isGetProductDetailDone,
  productDetail: state.Auth.productDetail,
});

const mapDispatchToProps = dispatch => ({
  onGetCategory: () => dispatch(actionGetCategory()),
  onGetProductDetail: (id) => dispatch(actionGetProductDetail(id)),
  onCreateProduct: (data) => dispatch(actionCreateProduct(data)),
});

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      isNumericString
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

class addProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCategory: '',
      name: '',
      sku: '',
      manufacturer: '',
      price: '',
      description: '',
      arrayImages: [],
      promotion: '',
      isDisable: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    let { isCreateDone, isGetProductDetailDone } = this.props;
    if (!isCreateDone && nextProps.isCreateDone) {
      history.push('/product');
    }
    console.log(nextProps)
    if (!isGetProductDetailDone && nextProps.isGetProductDetailDone) {
      let arrayImages = [];
      nextProps.productDetail.product_images.map(item => {
        arrayImages.push(item.image_url);
      });
      this.setState({
        name: nextProps.productDetail.name,
        selectedCategory: nextProps.productDetail.categories.length > 0 ? nextProps.productDetail.categories[0].id : '',
        manufacturer: nextProps.productDetail.manufacturer,
        description: nextProps.productDetail.description,
        price: nextProps.productDetail.price,
        sku: nextProps.productDetail.sku,
        arrayImages: arrayImages,
      }, () => {
        this._checkEnableButton();
      });

    }
  }

  componentDidMount() {
    let { onGetCategory, id, onGetProductDetail } = this.props;
    if (id) {
      onGetProductDetail(id);
    } else {
      onGetCategory();
    }
  }

  _handleChange = (e) => {
    this.setState({
      selectedCategory: e.target.value,
    });
  };

  _checkEnableButton = () => {
    let { selectedCategory, arrayImages, name, sku, manufacturer, price } = this.state;
    console.log(selectedCategory, arrayImages, name, sku, manufacturer, price);
    if (selectedCategory && arrayImages.length > 0 && name && sku && manufacturer && price) {
      this.setState({
        isDisable: false,
      });
    } else {
      this.setState({
        isDisable: true,
      });
    }
  };

  _handleSubmit = () => {
    let { description, arrayImages, name, sku, manufacturer, price, selectedCategory } = this.state;
    let { onCreateProduct, id } = this.props;
    onCreateProduct({
      description: description,
      name: name,
      sku,
      categories: selectedCategory.toString(),
      price,
      images: arrayImages,
      manufacturer: manufacturer,
      id
    });
  };

  _handleChangeInput = name => event => {

    this.setState({
      [name]: event.target.value,
    }, () => {
      this._checkEnableButton();
    });
  };

  removeImage = index => {
    let { arrayImages } = this.state;
    arrayImages.splice(index, 1);

    this.setState({
      arrayImages: arrayImages,
    });

    // arrayImages.splice(index, 1);
  };

  addImage = arrayImages => {
    this.setState({
      arrayImages: arrayImages,
    }, () => {
      this._checkEnableButton();
    });
  };

  render() {
    let isFullWidth = true;
    let { isLoading, arrayCategory, isCreating } = this.props;
    let { isDisable, promotion, selectedCategory, price, name, manufacturer, sku, description, arrayImages } = this.state;
    let isGrayBackground = true;
    return (
       <div>
         {
           isLoading ? (
             <div className="loading-container">
               <CircularProgress />
             </div>
           ) : (
             <Grid container direction="row" className="create-product-page" spacing={8}>
               <Grid item md={6}>
                 <TextField
                   required
                   fullWidth={isFullWidth}
                   id="standard-required"
                   label="Name"
                   margin="normal"
                   value={name}
                   onChange={this._handleChangeInput('name')}
                 />
                 <TextField
                   required
                   fullWidth={isFullWidth}
                   id="standard-required"
                   label="Manufacturer"
                   margin="normal"
                   value={manufacturer}
                   onChange={this._handleChangeInput('manufacturer')}
                 />
                 <TextField
                   required
                   fullWidth={isFullWidth}
                   id="standard-required"
                   label="Sku"
                   margin="normal"
                   value={sku}
                   onChange={this._handleChangeInput('sku')}
                 />
                 <TextField
                   required
                   fullWidth={isFullWidth}
                   id="standard-required"
                   label="Price"
                   margin="normal"
                   value={price}
                   onChange={this._handleChangeInput('price')}
                   InputProps={{
                     inputComponent: NumberFormatCustom,
                   }}
                 />
                 <FormControl fullWidth className="form-control">
                   <InputLabel id="demo-simple-select-label">Category</InputLabel>
                   <Select
                     labelId="demo-simple-select-label"
                     id="demo-simple-select"
                     value={selectedCategory}
                     onChange={this._handleChange}
                   >
                     {
                       arrayCategory.map((item) => {
                         return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                       })
                     }
                   </Select>
                 </FormControl>
                 {/*<TextField
                   fullWidth={isFullWidth}
                   id="standard-required"
                   label="Promotion"
                   margin="normal"
                   value={promotion}
                   onChange={this._handleChangeInput('promotion')}
                   InputProps={{
                     inputComponent: NumberFormatCustom,
                   }}
                 />*/}
                 <TextField
                   className="description"
                   fullWidth={isFullWidth}
                   id="standard-required"
                   label="Description"
                   margin="normal"
                   variant="outlined"
                   multiline
                   rows="6"
                   value={description}
                   onChange={this._handleChangeInput('description')}
                 />

               </Grid>
               <Grid item md={6}>
                 {
                   arrayImages.map((image, index) => {
                     return (
                       <div key={index} className="image-item">
                         <IconButton
                           onClick={(e) => {
                             e.stopPropagation();
                             this.removeImage(index);
                           }}
                           variant="outlined"
                           className="icon-container"
                         >
                           <CloseIcon className="icon-close" />
                         </IconButton>
                         <img className="image" src={image} alt="" />
                       </div>
                     );
                   })
                 }
                 <div className="button-file-container">
                   <label htmlFor="outlined-button-file">
                     <input
                       className="file-input"
                       accept="image/*"
                       id="outlined-button-file"
                       multiple
                       type="file"
                       onChange={(e) => {
                         e.preventDefault();

                         let reader = new FileReader();
                         let file = e.target.files[0];

                         reader.onloadend = () => {
                           let arrayImageClone = arrayImages;
                           arrayImageClone.push(reader.result);

                           this.addImage(arrayImageClone);
                         };

                         reader.readAsDataURL(file);
                         e.target.value = null;
                       }}
                     />
                     <IconButton
                       className="button"
                       variant="outlined"
                       component="span"
                     >
                       <Image
                         className="icon"
                         path="booking/button@3x"
                         extension="png"
                       />
                     </IconButton>
                   </label>
                 </div>
               </Grid>
               {
                 isCreating ? (
                   <div className="loading-container">
                     <CircularProgress />
                   </div>
                 ) : (
                   <Grid item md={4} className="button-container">
                     <Button
                       fullWidth
                       disabled={isDisable}
                       className="btn-submit"
                       variant="contained"
                       color="primary"
                       onClick={this._handleSubmit}
                     >
                       Send
                     </Button>
                   </Grid>
                 )
               }
             </Grid>
           )
         }
       </div>


    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(addProduct);
