import { GET_PRODUCT_DETAIL_REQUEST, CREATE_PRODUCT_REQUEST, LOGIN_FAIL, LOGIN_REQUEST, LOGIN_SUCCESS, APP_LOAD,
  GET_PRODUCT_REQUEST, GET_CATEGORY_REQUEST, APP_LOADED } from '../constants/actionTypes';

export const actionLogin = objectData => ({
  type: LOGIN_REQUEST,
  objectData,
});

export const actionCreateProduct = objectData => ({
  type: CREATE_PRODUCT_REQUEST,
  data: objectData,
});


export const actionLoadApp = token => ({
  type: APP_LOAD,
  token: token,
});

export const actionLoadedApp = token => ({
  type: APP_LOADED,
});

export const actionGetProductDetail = id => ({
  type: GET_PRODUCT_DETAIL_REQUEST,
  id,
});


export const actionGetProductList = token => ({
  type: GET_PRODUCT_REQUEST,
});

export const actionGetCategory = token => ({
  type: GET_CATEGORY_REQUEST,
});
